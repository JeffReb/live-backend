// env file
require('dotenv').config();
// Imports
var bcrypt = require('bcrypt');
var jwtUtils = require('../utils/jwt.utils');
var cassandra = require('cassandra-driver');
var asyncLib = require('async');

// env variables
const cassandraPass = process.env.CASSANDRA_PASS;
const cassandraContactPoint = process.env.CASSANDRA_CONTACT_POINT;
const cassandraKeyspace = process.env.CASSANDRA_KEYSPACE;


var authProvider = new cassandra.auth.PlainTextAuthProvider('cassandra', cassandraPass);


var client = new cassandra.Client({
    contactPoints: [cassandraContactPoint],
    localDataCenter: 'datacenter1',
    keyspace: cassandraKeyspace,
    authProvider: authProvider
});

client.connect(function (err, result) {
    console.log('index: cassandra connected');
});

// Constants
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const PASSWORD_REGEX = /^(?=.*\d).{4,12}$/;

// Routes
module.exports = {
    register: function(req, res){
        // TODO: To implement

        // Params
        var email = req.body.email;
        var name = req.body.name;
        var password = req.body.password;
        var id = cassandra.types.uuid();
        var saltRounds = 5;
        var bcryptedPassword =  bcrypt.hashSync(password, saltRounds);

        if(email === null || name === null || password === null){
            return res.status(400).json({'error': 'missing parameters'});
        }

        // TODO verify pseudo lenght, mail regex, password etc...
        if(name.lenght >= 13 || name.lenght <= 4){
            return res.status(400).json({'error': 'wrong username (must be lenght 5 - 12)'});
        }

        if(!EMAIL_REGEX.test(email)){
            return res.status(400).json({'error': 'email is not valid'});
        }

        if(!PASSWORD_REGEX.test(password)){
            return res.status(400).json({'error': 'password invalid (must lenght 4 - 12 and include 1 number at least'});
        }

        asyncLib.waterfall([
            function (done) {
                var getUserEmail = 'SELECT email FROM users_by_email WHERE email = ?';
                client.execute(getUserEmail, [email])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    return res.status(500).json({'error': 'unable to verify user email'});
                });
            },
            function(result, done){
                if(result.rowLength === 0){
                    done(null, result);
                } else {
                    return res.status(409).json({'error': 'email already exist'});
                }
            },
            function(result, done){
                var getUserName = 'SELECT id, name, email FROM users_by_name WHERE name = ?';
                client.execute(getUserName, [name])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    return res.status(500).json({'error': 'unable to verify username'});
                });
            },
            function(result, done){
                if (result.rowLength === 0){
                    done(null, result);
                } else {
                    return res.status(409).json({'error': 'name already exist'});
                }
            },
            function(result, done){
                var newUserbyEmail = 'INSERT INTO users_by_email(email, id, name, password, isvalidate) VALUES (?, ?, ?, ?, ?)';
                client.execute(newUserbyEmail,[email, id, name, bcryptedPassword, false])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    console.log(err);

                    return res.status(500).json({'error': 'cannot add user line 101'});
                });
            },
            function(result, done){
                var newUserbyName = 'INSERT INTO users_by_name(name, id, email, password) VALUES (?, ?, ?, ?)';
                client.execute(newUserbyName,[name, id, email, bcryptedPassword])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    return res.status(500).json({'error': 'cannot add user line 115'});
                });
            },
            function(result, done){
                var newUserbyId = 'INSERT INTO users_by_id(id, email, name, password) VALUES (?, ?, ?, ?)';
                client.execute(newUserbyId,[id, email, name, bcryptedPassword])
                .then(function(result){
                    done(result);
                })
                .catch(function(err){
                    return res.status(500).json({'error': 'cannot add user line 129'});
                });
            }
        ], function(result){
            if(result){
                return res.status(201).json({
                    'userId': id,
                    'name': name,
                    'email': email,
                    'isValidate': false,
                    'token': jwtUtils.generateTokenForUser({
                        id: id,
                        email: email,
                        name: name,
                        isValidate: false
                    })
                });
            } else {
                return res.status(500).json({'error': 'cannot add user line 136'});
            }
        });
    },
    login: function(req, res) {
        // TODO: To implement

        // Params
        var email = req.body.email;
        var password = req.body.password;

        if(email === null || password === null){
            return res.status(400).json({'error': 'missing parameters'});
        }

        // TODO verify mail regex, password etc...
        asyncLib.waterfall([
            function(done){
                var getUserEmail = 'SELECT email, password FROM users_by_email WHERE email = ?';
                client.execute(getUserEmail, [email])
                .then(function(result){
                    done(null, result);
                })
                .catch(function(err){
                    return res.status(500).json({'error': 'unable to verify user email'});
                });
            },
            function(result, done){
                if(result.rowLength === 1){
                    bcrypt.compare(password, result.rows[0].password, function(errBycrypt, resBycrypt){
                        done(null, result, resBycrypt);
                    });
                } else {
                    return res.status(404).json({'error': 'user not found'});
                }
            },
            function(result, resBycrypt, done){
                if(resBycrypt){
                    done(result);
                } else {
                    return res.status(403).json({'error': 'invalid password'});
                }
            }
        ], function(result){
            if(result){
                var getUserId = 'SELECT id, email, name, isvalidate FROM users_by_email WHERE email = ?';
                client.execute(getUserId, [email], function (err, result){
                    if (err){
                        return res.status(500).json({'error': 'unable to generate token'});
                    } else {
                        var idToken = result.rows[0].id;
                        var emailToken = result.rows[0].email;
                        var nameToken = result.rows[0].name;
                        var isValidateToken = result.rows[0].isvalidate;
                        return res.status(201).json({
                            'userId': idToken,
                            'name': nameToken,
                            'email': emailToken,
                            'isValidate': isValidateToken,
                            'token': jwtUtils.generateTokenForUser({
                                id: idToken,
                                email: emailToken,
                                name: nameToken,
                                isValidate: isValidateToken
                            })
                        });
                    }
                });
            } else {
                return res.status(500).json({'error': 'cannot log on user'});
            }
        });

        
    },
    getStatus: function (req, res) {
        // Getting auth header
        var headerAuth = req.headers['authorization'];
        var userId = jwtUtils.getUserId(headerAuth);

        userToken = headerAuth.replace('Bearer ', '');


        if (userId < 0){
            return res.status(401).json({'error': 'wrong token'});
        } else {
            var user = jwtUtils.getUser(headerAuth);
            var id = user.userId;
            var email = user.userEmail;
            var name = user.userName;
            var isValidate = user.isValidate;

            
            return res.status(201).json({
                'userId': id,
                'name': name,
                'email': email,
                'isValidate': isValidate,
                'token': userToken
            });
        }
            

        
    }
}
