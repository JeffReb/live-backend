Master Branch:

[![pipeline status](https://gitlab.com/JeffReb/live-backend/badges/master/pipeline.svg)](https://gitlab.com/JeffReb/live-backend/commits/master)


Develop Branch:

[![pipeline status](https://gitlab.com/JeffReb/live-backend/badges/develop/pipeline.svg)](https://gitlab.com/JeffReb/live-backend/commits/develop)





### install express, jsonwebtoken, bcrypt, body-parser, async and cassandra-driver

Global in local machine:
npm install -g express-cli
npm install -g nodemon

Project:
npm install express --save
npm install jsonwebtoken --save
npm install bcrypt --save
npm install body-parser
npm install async
npm install cassandra-driver

bodyParser c'est ce qui permet de récupérer les arguments et les paramètres fournis
dans le body d'une requete http

bcrypt encritage des mots de passe

jsonwebtoken permet de gérer sous forme de passeport les connexions autorisés a l'API après login

async permet de gérer les waterfalls (amélioration du code en cascade sur les call back)

cassandra-driver connexion a la base de données cassandra

### install, connect and setting CASSANDRA

1. in kubernetes online installed via rancher from bitnami image with setting:
volumePermissions.enabled=true persistence.storageClass=managed-nfs-storage namespace cassandra-dev

2. in local vagrant via helm on kubernetes cluster:

```sh
helm install --set volumePermissions.enabled=true --set persistence.storageClass=managed-nfs-storage --namespace cassandra --name cassandra bitnami/cassandra
```

3. Check the cluster status by running (see exact command line on notes from helm install or via rancher):
```sh
kubectl exec -it --namespace cassandra $(kubectl get pods --namespace cassandra -l app=cassandra,release=cassandra -o jsonpath='{.items[0].metadata.name}') nodetool status
```

4. export in variable environment password
```sh
export CASSANDRA_PASSWORD=$(kubectl get secret --namespace cassandra cassandra -o jsonpath="{.data.cassandra-password}" | base64 --decode)  <= in cassandra vagrant

export CASSANDRA_PASSWORD=$(kubectl get secret --namespace cassandra cassandra-dev -o jsonpath="{.data.cassandra-password}" | base64 --decode)  <= in cassandra k8s online with develop version of cassandra

```

NOTE THE PASSWORD OF CASSANDRA WITH printenv

5. Run a Cassandra pod that you can use as a client To connect to your Cassandra cluster using CQL:

```sh
kubectl run --namespace cassandra cassandra-client --rm --tty -i --restart='Never' --env CASSANDRA_PASSWORD=$CASSANDRA_PASSWORD --image docker.io/bitnami/cassandra:3.11.4-debian-9-r147 -- bash  <= vagrant verison

kubectl run --namespace cassandra cassandra-dev-client --rm --tty -i --restart='Never' --env CASSANDRA_PASSWORD=$CASSANDRA_PASSWORD --image docker.io/bitnami/cassandra:3.11.4-debian-9-r147 -- bash  <= online version
```

6. Connect using the cqlsh client:

```sh
cqlsh -u cassandra -p <password-cassandra> cassandra-dev // <= online version
cqlsh -u cassandra -p <password-cassandra> cassandra // <= vagrant version
```

7. To connect to your database from outside the cluster execute the following commands:

```sh
kubectl port-forward --namespace cassandra svc/cassandra 9042:9042  <= vagrant version

kubectl port-forward --namespace cassandra svc/cassandra-dev 9042:9042  <= online version

// create ssh tunel with this port
ssh -L 9042:127.0.0.1:9042 vagrant@vag

ssh -L 9042:127.0.0.1:9042 jeff@online -p 2223
```

8. connect and set your cassandra cluster via TablePlus outside the cluster

in tableplus create a connection via http://127.0.0.1:9042 user: cassandra password: <password-cassandra>

9. create keyspace via ssh cqlsh (see: https://stph.scenari-community.org/contribs/nos/Cassandra1/co/langage_CQL.html)

```sh
cassandra@cqlsh> DESCRIBE KEYSPACES; // only via cassandra pod client
cassandra@cqlsh> CREATE KEYSPACE <keyspace_name> WITH REPLICATION = { 'class' : '<class de la strategie>', 'replication_factor' : <nb réplication> }; // CREATE KEYSPACE back_ WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 }; // only via cassandra pod client

cassandra@cqlsh> USE people; //via TablePlus
cassandra@cqlsh:people> //via TablePlus

cassandra@cqlsh:people> CREATE TABLE subscribers (id uuid PRIMARY KEY, email text, first_name text, last_name text); // Run via TablePlus in SQL command because your are must secure with the commit fonction and you can test first in another test keyspace
```

10. clean cassandra after deleting keyspace

run nodetools in cassandra client (see in part 5 to launch pod cassandra client)


see: https://docs.datastax.com/en/archived/cassandra/3.0/cassandra/tools/toolsNodetool.html

11. Modelyze your database like this:

```sh
CREATE TABLE users (
    id uuid PRIMARY KEY,
    username text,
    email text,
    age int
)
 
CREATE TABLE users_by_username (
    username text PRIMARY KEY,
    id uuid
)
 
CREATE TABLE users_by_email (
    email text PRIMARY KEY,
    id uuid
)
```

see recommendation of this basic rules: 

https://www.datastax.com/dev/blog/basic-rules-of-cassandra-data-modeling

https://stph.scenari-community.org/contribs/nos/Cassandra1/co/modele_de_donnees.html

http://cassandra.apache.org/doc/latest/cql/types.html#data-types

http://jffourmond.github.io/2017/06/18/choses-a-savoir-avant-de-debuter-en-cassandra/

https://blog.jetoile.fr/2012/03/petits-retours-sur-cassandra.html

https://ippon.developpez.com/tutoriels/nosql/cassandra/modelisation-cassandra/


12. use postman to test your API

```sh
adresses:

feature:
https://feature.meta.live-assault.com

staging:
https://staging.meta.live-assault.com

prod:
https://meta.live-assault.com


```

### prepare your database for recept records


```sh

DESCRIBE KEYSPACES;

USE live_assault_<your branch>; // feat or staging or prod

CREATE TABLE users_by_email (email text PRIMARY KEY, id uuid, password text, name text, isvalidate boolean);
CREATE TABLE users_by_name (name text PRIMARY KEY, id uuid, password text, email text);
CREATE TABLE users_by_id (id uuid PRIMARY KEY, name text, password text, email text);

// for update:

ALTER TABLE users_by_email ADD isValidate boolean;
```










