Develop branch:

[![pipeline status](https://gitlab.com/JeffReb/live-assault/badges/develop/pipeline.svg)](https://gitlab.com/JeffReb/live-assault/commits/develop)
[![coverage report](https://gitlab.com/JeffReb/live-assault/badges/develop/coverage.svg)](https://gitlab.com/JeffReb/live-assault/commits/develop)
[![Known Vulnerabilities](https://snyk.io//test/github/jeffAerials/live-assault/badge.svg?targetFile=package.json)](https://snyk.io//test/github/jeffAerials/live-assault?targetFile=package.json)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
[![StackShare](http://img.shields.io/badge/tech-stack-0690fa.svg?style=flat)](https://stackshare.io/JeffReb/live-front)

Beta branch:

[![pipeline status](https://gitlab.com/JeffReb/live-assault/badges/beta/pipeline.svg)](https://gitlab.com/JeffReb/live-assault/commits/beta)
[![coverage report](https://gitlab.com/JeffReb/live-assault/badges/beta/coverage.svg)](https://gitlab.com/JeffReb/live-assault/commits/beta)
[![Known Vulnerabilities](https://snyk.io//test/github/jeffAerials/live-assault/badge.svg?targetFile=package.json)](https://snyk.io//test/github/jeffAerials/live-assault?targetFile=package.json)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
[![StackShare](http://img.shields.io/badge/tech-stack-0690fa.svg?style=flat)](https://stackshare.io/JeffReb/live-front)

Master:

[![pipeline status](https://gitlab.com/JeffReb/live-assault/badges/master/pipeline.svg)](https://gitlab.com/JeffReb/live-assault/commits/master)
[![coverage report](https://gitlab.com/JeffReb/live-assault/badges/master/coverage.svg)](https://gitlab.com/JeffReb/live-assault/commits/master)  
[![Known Vulnerabilities](https://snyk.io//test/github/jeffAerials/live-assault/badge.svg?targetFile=package.json)](https://snyk.io//test/github/jeffAerials/live-assault?targetFile=package.json)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
[![StackShare](http://img.shields.io/badge/tech-stack-0690fa.svg?style=flat)](https://stackshare.io/JeffReb/live-front)

# DOCUMENTATION OF THE CONSTRUCTION AND ORGANISATION OF THE PROJECT

# Git Flow

![](img/gitflow.png)

## Commits rules

a. First, 1 commit by feature

b. commit title and description rules

```sh
    <type>:<subject>
    <description>
    example:
    feat:Add Sass instead of css
    Add npm Sass dependencies and replace in
    angular-cli.json the stylesheet type in by sass

```

for this we use precommit git hook

### Git Hook in precommit

this project is set to use TSLint before commit and test commit rules

if your code don't pass TSLint and commit rules your commit action are not executed

we use husky npm package installed in this project to setup this fonction

we use commitlint npm package to setup lint commit message in pre-commit

you can see rules below:
see: https://commitlint.js.org/#/reference-rules
  
  
 `` sh - `feat`: (new feature for the user, not a new feature for build script) - `fix`: (bug fix for the user, not a fix to a build script) - `docs`: (changes to the documentation) - `style`: (formatting, missing semi colons, etc; no production code change) - `refactor`: (refactoring production code, eg. renaming a variable) - `test`: (adding missing tests, refactoring tests; no production code change) - `chore`: (updating grunt tasks etc; no production code change) - `build`: (Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)) - `ci`: Changes to our CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs) ``
