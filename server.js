// env file
require('dotenv').config();
// Imports
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var apiRouter = require('./apiRouter').router;

// Instantiate server
var server = express();

server.use(cors()); // allow access-control-allow-origin

// env variables
const cassandraPass = process.env.CASSANDRA_PASS;
const cassandraContactPoint = process.env.CASSANDRA_CONTACT_POINT;
const cassandraKeyspace = process.env.CASSANDRA_KEYSPACE;




// Body Parser configuration
server.use(bodyParser.urlencoded({extended: true}));
server.use(bodyParser.json());


// Configure routes
server.get('/', function(req, res){
    res.setHeader('Content-Type', 'text/html');
    res.status(200).send(`<h1>Bonjour sur mon serveur</h1>`);
});

// assign apiRouter
server.use('/api/', apiRouter);

// Launch server
server.listen(5000, function(){
    console.log('Server en écoute :)');
    console.log(cassandraPass);
    console.log(cassandraContactPoint);
    console.log(cassandraKeyspace);
});